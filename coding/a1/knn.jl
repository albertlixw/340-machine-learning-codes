include("misc.jl") # Includes GenericModel typedef

function knn_predict(Xhat,X,y,k)
  (n,d) = size(X)
  (t,d) = size(Xhat)
  k = min(n,k) # To save you some debuggin

  #Define yhat as the output labels, distas as Euclidien distance
  yhat = zeros(t)
  dists = zeros(n)

  #Compute Euclidien distance of test data
  for i in 1:t
    for j in 1:n
      dists[j] = norm(Xhat[i,:] - X[j,:])
    end

    # Sort the distances by ascending
    indices = sortperm(dists)

    #Get the indices of first k neighbours
    indices = indices[1:k]
    #Get the most common labels in k neighbours
    yneighbours = y[indices]
    # yneighbours = zeros(k)
    # for m in 1:k
    #   index = round(Int,indices[m])
    #   yneighbours[m] = y[index]
    # end

    label = mode(yneighbours)

    # Store the labes into yhat
    yhat[i] = label

    # reset distance
    dists = zeros(n)
    yneighbours = zeros(k)
  end

  #return fill(yhat,t)

  # Now that we have the best rule,
  # let's build our predict function
  function predict(Xhat)
    return yhat
  end

  return yhat
  #return fill(yhat,t)
end




function knn(X,y,k)
	# Implementation of k-nearest neighbour classifier
  predict(Xhat) = knn_predict(Xhat,X,y,k)
  return GenericModel(predict)
end

function cknn(X,y,k)
	# Implementation of condensed k-nearest neighbour classifier
	(n,d) = size(X)
	Xcond = X[1,:]'
	ycond = [y[1]]
	for i in 2:n
    		yhat = knn_predict(X[i,:]',Xcond,ycond,k)
    		if y[i] != yhat[1]
			Xcond = vcat(Xcond,X[i,:]')
			push!(ycond,y[i])
    		end
	end

	predict(Xhat) = knn_predict(Xhat,Xcond,ycond,k)
	return GenericModel(predict)
end
