# Load X and y variable
include("dataprocessing.jl")

using JLD
X = load("citiesSmall.jld","X")
y = load("citiesSmall.jld","y")
Xtest = load("citiesSmall.jld","Xtest")
ytest = load("citiesSmall.jld","ytest")

# Fit a KNN classifier
k = 3
include("knn.jl")
model = knn(X,y,k)
#model = cknn(X,y,k)

# Evaluate training error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@printf("Train Error with %d-nearest neighbours: %.3f\n",k,trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with %d-nearest neighbours: %.3f\n",k,testError)


# # Plot classifier
# include("plot2Dclassifier.jl")
# plot2Dclassifier(X,y,model)
# plot2Dclassifier(Xtest,ytest,model)


X = normalizeData(X)
#y = normalizeData(Y)
Xtest = normalizeData(Xtest)
#ytest = normalizeData(ytest)

# Fit a KNN classifier
k = 3
include("knn.jl")
model = knn(X,y,k)
#model = cknn(X,y,k)

# Evaluate training error
yhat = model.predict(X)
trainError = mean(yhat .!= y)
@printf("Train Error after standarlization with %d-nearest neighbours: %.3f\n",k,trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error after standarlization with %d-nearest neighbours: %.3f\n",k,testError)
