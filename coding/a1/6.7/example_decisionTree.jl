# Load X and y variable
include("dataprocessing.jl")

using JLD
X = load("citiesSmall.jld","X")
y = load("citiesSmall.jld","y")
y2 = y
Xtest = load("citiesSmall.jld","Xtest")
ytest = load("citiesSmall.jld","ytest")
n = size(X,1)

# Fit a decision tree and compute error
include("decisionTree.jl")
depth = 2
model = decisionTree(X,y,depth)

# Evaluate training error
yhat = model.predict(X)
trainError = sum(yhat .!= y)/n
@printf("Error with depth-%d decision tree: %.3f\n",depth,trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with depth-%d decision tree: %.3f\n",k,testError)

X = normalizeData(X)
Xtest = normalizeData(Xtest)
n = size(X,1)

# Fit a decision tree and compute error
include("decisionTree.jl")
depth = 2
model = decisionTree(X,y,depth)

# Evaluate training error
yhat = model.predict(X)
trainError = sum(yhat .!= y)/n
@printf("Error with depth-%d decision tree: %.3f\n",depth,trainError)

# Evaluate test error
yhat = model.predict(Xtest)
testError = mean(yhat .!= ytest)
@printf("Test Error with depth-%d decision tree: %.3f\n",k,testError)
