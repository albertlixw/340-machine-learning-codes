# standarlization of features
# for each column of X, we substract its mean and divide by its variance

function normalizeData(X)
    (n,d)  = size(X)
    X_mean = zeros(d)
    X_var = zeros(d)

    for j in 1:d
        X_mean[j] = mean(X[:,j])
        X_var[j] = var(X[:,j])
    end

    for i in 1:n
        for j in 1:d
            temp = X[i,j] - X_mean[j]
            if(X_var[j] == 0)
                temp = 0;
            else
                temp = temp/X_var[j]
            end
            X[i,j] = temp
            #X[i,j] = (X[i,j] - X_mean[j])/X_var[j]
        end
    end

    # # println(X)
    #  println("X_mean is ", X_mean)
    #  println("X_var is ", X_var)
    return X
end
