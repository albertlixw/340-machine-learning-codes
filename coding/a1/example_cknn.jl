# Load X and y variable
using JLD
X = load("citiesBig1.jld","X")
y = load("citiesBig1.jld","y")
Xtest = load("citiesBig1.jld","Xtest")
ytest = load("citiesBig1.jld","ytest")


# Fit a KNN classifier
k = 1
include("knn.jl")
model = knn(X,y,k)

# Evaluate training error prediction time
println("KNN: prediction for training error")
@time(model.predict(X))

# Evaluate test error prediction time
println("KNN:prediction for test error")
@time(model.predict(Xtest))


model2 = cknn(X,y,k)

# Evaluate training error prediction time
println("CNN: prediction for training error")
@time(model2.predict(X))

# Evaluate test error prediction time
println("CNN: prediction for test error")
@time(model2.predict(Xtest))
