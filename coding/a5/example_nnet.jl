# Load X and y variable
using JLD
using PyPlot
data = load("basisData.jld")
(X,y) = (data["X"],data["y"])
n = size(X,1)

#standarize X and y
X = [ones(n,1) X]

#X_Original = X
#tempX = standardizeCols(X)
#X = [ones(n,1) tempX[1]]

y_Original = y
tempy = standardizeCols(y)
y = tempy[1]

d = 2

# Choose network structure and randomly initialize weights
include("NeuralNet.jl")
#nHidden = [3]
nHidden = [12]
nParams = NeuralNet_nParams(d,nHidden)
w = randn(nParams,1)

# Train with stochastic gradient
#maxIter = 10000
#stepSize = 1e-4
stepSize = 0.01
maxIter = 100000
for t in 1:maxIter

	# The stochastic gradient update:
	i = rand(1:n)
	(f,g) = NeuralNet_backprop(w,X[i,:],y[i],nHidden)
	w = w - stepSize*g

	# Every few iterations, plot the data/model:
	if (mod(t-1,round(maxIter/50)) == 0)
		@printf("Training iteration = %d\n",t-1)
		figure(1)
		clf()
		Xhat = -10:.05:10
		yhat = NeuralNet_predict(w,[ones(length(Xhat),1) Xhat],nHidden)
		plot(X[:,2],y,".")
		#plot(X_Original[:,1],y_Original,".")
		plot(Xhat,yhat,"g-")
		sleep(.1)
	end
end
